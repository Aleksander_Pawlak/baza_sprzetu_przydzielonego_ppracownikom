﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ModyfikujNarzedzia.aspx.cs" Inherits="Baza_sprzetu_przydzielonego_pracownikom.ModyfikujNarzedzia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <head>
        <style>
            .grid-container {
                display: grid;
                grid-template-columns: auto auto auto;
                background-color: #2196F3;
                padding: 10px;
                margin-left: 160px
            }

            .grid-item {
                background-color: rgba(255, 255, 255, 0.8);
                border: 1px solid rgba(0, 0, 0, 0.8);
                padding: 20px;
                font-size: 30px;
                text-align: center;
            }
        </style>
        <script language="C#" runat="server">

      void LinkButton_Click(Object sender, EventArgs e) 
      {
             Response.Redirect("ModyfikacjaNarzedziaSzczegoly.aspx");
         
      }

   </script>
    </head>
    <body>
        <div class="grid-container">

            <asp:DataList ID="DataList1"  runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" DataKeyField="Id" DataSourceID="SqlDataSource1" Width="543px">
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <ItemTemplate>
                    Id:
                    <asp:Label ID="IdLabel" runat="server" Text='<%#Eval("Id") %>'  />       
                    <br />
                    nazwa:
                    <asp:Label ID="nazwaLabel" runat="server" Text='<%# Eval("nazwa") %>' />
                    <br />
                    Typtyp:
                    <asp:Label ID="TyptypLabel" runat="server" Text='<%# Eval("Typtyp") %>' />
                    <br />

                    <a href="/RezerwacjaNarzedzia?idTool=<%# Eval("Id") %>">Szczegoly</a>
                    
                    <br />
                </ItemTemplate>
                <SelectedItemStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            </asp:DataList>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Dodaj" OnClick="Button1_Click" />

        </div>

    </body>
    </html>

</form>
</asp:Content>
