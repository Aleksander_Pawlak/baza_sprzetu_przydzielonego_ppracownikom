﻿using Baza_sprzetu_przydzielonego_pracownikom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Baza_sprzetu_przydzielonego_pracownikom
{
    public partial class RezerwacjaNarzedzia : System.Web.UI.Page
    {
        Narzedzie d;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            String s = Request.QueryString["idTool"];
            d = NarzedzieContext.GetToolById(int.Parse(s));
            Info.Rows[0].Cells[0].Text = "Nazwa: ";
            Info.Rows[0].Cells[1].Text = d.nazwa;
            Info.Rows[1].Cells[0].Text = "Typ: ";
            Info.Rows[1].Cells[1].Text = d.Typtyp;
            Info.Rows[4].Cells[0].Text = "Parametry: ";
            Info.Rows[4].Cells[1].Text = d.parametry;
            Info.Rows[2].Cells[0].Text = "Waga: ";
            Info.Rows[2].Cells[1].Text = d.waga;
            Info.Rows[3].Cells[0].Text = "ID: ";
            Info.Rows[3].Cells[1].Text = d.Id.ToString();
         
                PobierzZdaj.Visible = RezerwacjaContext.GetToolReservations(d.Id).Last().od_kiedy < DateTime.Today;
                if (d.dostepnosc == true)
                    PobierzZdaj.Text = "Pobierz";
                else
                    PobierzZdaj.Text = "Zdaj";
            }
            catch(Exception ex)
            {
                
                if (d.dostepnosc == true)
                    PobierzZdaj.Text = "Pobierz";
                else
                    PobierzZdaj.Text = "Zdaj";
            }
           

        }

        protected void adminDodajCertyfikat_Click(object sender, EventArgs e)
        {

        }

        protected void DodajZamieńNarzędzieAdmin_Click(object sender, EventArgs e)
        {
            Response.Redirect("ModyfikujNarzedzia.aspx");
            
        }

        protected void RezerwujModyfikuj_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (RezerwacjaContext.GetToolReservations(d.Id).Last().od_kiedy > dataOd.SelectedDate)
                {
                    RezerwacjaContext.changeReservationDate(d.Id, dataOd.SelectedDate);
                }
                else
                {
                    RezerwacjaContext.AddReservation("szcz761@gmail.com", d.Id, dataOd.SelectedDate);
                }
            }
            catch(Exception ee)
            {
                RezerwacjaContext.AddReservation("szcz761@gmail.com", d.Id, dataOd.SelectedDate);
            }
        }



        protected void PobierzZdaj_Click(object sender, EventArgs e)
        {
            try
            {
                if (RezerwacjaContext.GetToolReservations(d.Id).Last().od_kiedy < DateTime.Today)
            {
                PobierzZdaj.Visible = true;

                if (d.dostepnosc == true)
                {
                    PobierzZdaj.Text = "Pobierz";
                }
                else
                {
                    PobierzZdaj.Text = "Zdaj";
                }

                WypozyczenieContext.AddBorrowing("szcz761@gmail.com", d.Id, DateTime.Today, dataDo.SelectedDate);
            }
            else
            {
                PobierzZdaj.Visible = false;
            }
            }
            catch (Exception ee)
            {

            }
        }

        protected void ZgłośUsterkę_Click(object sender, EventArgs e)
        {
            UsterkaContext.AddFault("szcz761@gmail.com", d.Id, "zepsute");
        }

        protected void UsunUsterkeAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                UsterkaContext.DeleteFalut(UsterkaContext.GetToolFaults(d.Id).Last().Id);
            }
            catch(Exception ewe)
            {

            }
        }
    }
}