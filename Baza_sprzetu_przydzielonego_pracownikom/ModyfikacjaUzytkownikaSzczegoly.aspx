﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ModyfikacjaUseraSzczegoly.aspx.cs" Inherits="Baza_sprzetu_przydzielonego_pracownikom.ModyfikacjaUsera" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
                <head>
<style>
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto;
  background-color: #2196F3;
  padding: 10px;
  margin-left: 160px
}
.grid-item {
  background-color: rgba(255, 255, 255, 0.8);
  border: 1px solid rgba(0, 0, 0, 0.8);
  padding: 20px;
  font-size: 30px;
  text-align: center;
}
</style>
</head>
    <body>
        <div class="grid-container">

    <asp:Label ID="Label1" runat="server" style="text-align: right" Text="Imię"></asp:Label>
    <asp:TextBox ID="imie" runat="server" Width="162px"></asp:TextBox>
    <br />
    <asp:Label ID="Label2" runat="server" style="text-align: right" Text="Nazwisko"></asp:Label>
    <asp:TextBox ID="nazwisko" runat="server" Width="129px"></asp:TextBox>
    <br />
    <asp:Label ID="Label3" runat="server" style="text-align: right" Text="ID"></asp:Label>
    <asp:TextBox ID="id" runat="server" Width="172px"></asp:TextBox>
    <br />
    <asp:Label ID="Label4" runat="server" style="text-align: right" Text="Stanowisko"></asp:Label>
    <asp:TextBox ID="stanowisko" runat="server" Width="114px"></asp:TextBox>
    <br />
    <asp:Label ID="Label5" runat="server" style="text-align: right" Text="Poziom uprawnień"></asp:Label>
    <asp:ListBox ID="ListBox1" runat="server" Height="19px" Width="108px"></asp:ListBox>
    <br />
    <asp:Label ID="Label6" runat="server" Text="Telefon"></asp:Label>
    <asp:TextBox ID="telefon" runat="server" Width="138px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label7" runat="server" Text="userRole"></asp:Label>
    <br />
    <asp:CheckBoxList ID="CheckBoxList1" runat="server">
    </asp:CheckBoxList>
    <br />
    <asp:Button ID="Button1" runat="server" Text="Dodaj" />
    <asp:Button ID="Button2" runat="server" Text="Usuń zaznaczone" />
    <br />
    <br />
    <asp:Label ID="Label8" runat="server" Text="Wyjątki"></asp:Label>
    <br />
    <asp:CheckBoxList ID="CheckBoxList2" runat="server">
    </asp:CheckBoxList>
    <br />
    <asp:Button ID="Button3" runat="server" Text="Dodaj" />
    <asp:Button ID="Button4" runat="server" Text="Usuń zaznaczone" />
    <br />
    <br />
    <asp:Label ID="Label9" runat="server" Text="Historia wypożyczeń"></asp:Label>
    <br />
    <asp:DataList ID="DataList1" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
        <SelectedItemStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
    </asp:DataList>
    <br />
    <br />
    <asp:Label ID="Label10" runat="server" Text="Aktywne rezerwacje"></asp:Label>
    <br />
    <asp:CheckBoxList ID="CheckBoxList3" runat="server">
    </asp:CheckBoxList>
    <br />
    <asp:Button ID="Button5" runat="server" Text="Usuń zaznaczone" />
    <br />
    <br />
    <asp:Button ID="Button6" runat="server" Text="Dodaj/Zmień" />
    <asp:Button ID="Button7" runat="server" Text="Usuń" />

                        </div>

</body>
</html>

</form>
</asp:Content>
