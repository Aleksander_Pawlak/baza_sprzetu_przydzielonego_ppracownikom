﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Baza_sprzetu_przydzielonego_pracownikom.Startup))]
namespace Baza_sprzetu_przydzielonego_pracownikom
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
