//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Baza_sprzetu_przydzielonego_pracownikom
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            this.Rezerwacjas = new HashSet<Rezerwacja>();
            this.Wypozyczenies = new HashSet<Wypozyczenie>();
            this.Zabranie_materialu = new HashSet<Zabranie_materialu>();
            this.Usterkas = new HashSet<Usterka>();
            this.Grupa_uprawnien = new HashSet<Grupa_uprawnien>();
        }
    
        public string imie { get; set; }
        public string nazwisko { get; set; }
        public string PESEL { get; set; }
        public int wiek { get; set; }
        public string email { get; set; }
        public int telefon { get; set; }
        public string haslo { get; set; }
        public string userRole { get; set; }
        public int Adresid { get; set; }
    
        public virtual Adres Adre { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rezerwacja> Rezerwacjas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Wypozyczenie> Wypozyczenies { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Zabranie_materialu> Zabranie_materialu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usterka> Usterkas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Grupa_uprawnien> Grupa_uprawnien { get; set; }
    }
}
