﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class DostawaNarzedziaContext
    {
        //TODO: REFACTOR
        public static Dostawa_narzedzia AddToolSupply(int supplierNIP, int toolId, DateTime date, int number)
        {
            var delivery = new Dostawa_narzedzia()
            {
                data = date,
                ilosc = number
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supplier = db.Dostawcas.First(c => c.NIP == supplierNIP);
                var tool = db.Narzedzies.First(c => c.Id == toolId);

                delivery.Dostawca = supplier;
                delivery.Narzedzie = tool;

                AddOldToolsDependency(tool, supplier, delivery, db);
                AddNewTools(tool, delivery, db);

                db.Dostawa_narzedzia.Add(delivery);
                db.SaveChanges();
            }

            return delivery;
        }

        private static void AddOldToolsDependency(Narzedzie tool, Dostawca supplier, Dostawa_narzedzia delivery,
            Baza_sprzetu_przydzielonego_pracownikomEntities db)
        {
            IList<Narzedzie> equalTools = db.Narzedzies.Where(c => c.nazwa == tool.nazwa && c.Id != tool.Id).ToList();
            foreach (var eqTool in equalTools)
            {
                var toolDelivery = new Dostawa_narzedzia()
                {
                    data = delivery.data,
                    ilosc = delivery.ilosc
                };

                toolDelivery.Dostawca = supplier;
                toolDelivery.Narzedzie = eqTool;

                db.Dostawa_narzedzia.Add(toolDelivery);
            }
        }

        /*private static void RemoveOldDependency(int toolId, int supplierNIP,
            Baza_sprzetu_przydzielonego_pracownikomEntities db)
        {
            var dependency = db.Dostawa_narzedzia.FirstOrDefault(c => c.DostawcaNIP == supplierNIP &&
                c.NarzedzieId == toolId);

            if (!EqualityComparer<Dostawa_narzedzia>.Default.Equals(dependency, default(Dostawa_narzedzia)))
            {
                db.Dostawa_narzedzia.Remove(dependency);
                db.SaveChanges();
            }
        }*/

        /*private static void RemoveOldToolsDependency(Narzedzie tool, Dostawca supplier, Dostawa_narzedzia delivery,
            Baza_sprzetu_przydzielonego_pracownikomEntities db)
        {
            IList<Narzedzie> equalTools = db.Narzedzies.Where(c => c.nazwa == tool.nazwa && c.Id != tool.Id).ToList();
            foreach (var eqTool in equalTools)
            {
                RemoveOldDependency(eqTool.Id, supplier.NIP, db);

                var toolDelivery = new Dostawa_narzedzia()
                {
                    data = delivery.data,
                    ilosc = delivery.ilosc
                };

                toolDelivery.Dostawca = supplier;
                toolDelivery.Narzedzie = eqTool;

                db.Dostawa_narzedzia.Add(toolDelivery);
            }
        }*/

        private static void AddNewTools(Narzedzie tool, Dostawa_narzedzia delivery,
            Baza_sprzetu_przydzielonego_pracownikomEntities db)
        {
            for (int i = 0; i < delivery.ilosc; i++)
            {
                var newTool = new Narzedzie()
                {
                    nazwa = tool.nazwa,
                    parametry = tool.parametry,
                    dostepnosc = true,
                    rozmiar = tool.rozmiar,
                    waga = tool.waga,
                    stan = "nowe"
                };

                newTool.Typ = tool.Typ;
                db.Narzedzies.Add(newTool);
                db.SaveChanges();

                var tmpdelivery = new Dostawa_narzedzia()
                {
                    data = delivery.data,
                    ilosc = delivery.ilosc
                };

                tmpdelivery.Dostawca = delivery.Dostawca;
                tmpdelivery.Narzedzie = newTool;
                db.Dostawa_narzedzia.Add(tmpdelivery);
            }
        }

        /*public static void DeleteSupply(int nip, int toolId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supply = db.Dostawa_narzedzia.First(c => c.DostawcaNIP == nip &&
                    c.NarzedzieId == toolId);

                db.Dostawa_narzedzia.Remove(supply);
                db.SaveChanges();
            }
        }*/

        public static void DeleteSupply(int supplyId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supply = db.Dostawa_narzedzia.First(c => c.Id == supplyId);

                db.Dostawa_narzedzia.Remove(supply);
                db.SaveChanges();
            }
        }

        public static List<Dostawa_narzedzia> GetAllSupplierSupply(int nip)
        {
            List<Dostawa_narzedzia> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Dostawa_narzedzia.Where(c => c.DostawcaNIP == nip).ToList();
            }

            return list;
        }

        public static List<Dostawa_narzedzia> GetAllMaterialSupply(int toolId)
        {
            List<Dostawa_narzedzia> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Dostawa_narzedzia.Where(c => c.NarzedzieId == toolId).ToList();
            }

            return list;
        }

        /*public static void UpdateSupplyDate(int supplierNIP, int toolId, System.DateTime newDate)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supply = db.Dostawa_narzedzia.First(c => c.DostawcaNIP == supplierNIP && c.NarzedzieId == toolId);

                supply.data = newDate;
                db.SaveChanges();
            }
        }*/

        public static void UpdateSupplyDate(int supplyId, System.DateTime newDate)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supply = db.Dostawa_narzedzia.First(c => c.Id == supplyId);

                supply.data = newDate;
                db.SaveChanges();
            }
        }
    }
}