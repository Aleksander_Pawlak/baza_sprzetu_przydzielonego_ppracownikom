﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class WypozyczenieContext
    {
        public static Wypozyczenie AddBorrowing(string userEmail, int toolId, DateTime from, DateTime to)
        {
            //RemoveOldDependency(toolId, userId);

            var borrow = new Wypozyczenie()
            {
                od_kiedy = from,
                do_kiedy = to
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var user = db.Users.First(c => c.email == userEmail);
                var tool = db.Narzedzies.First(c => c.Id == toolId);

                if (!tool.dostepnosc)
                    throw new InvalidOperationException("Narzedzie jest aktualnie niedostępne.");

                bool allowed = tool.Grupa_uprawnien.Any(c => user.Grupa_uprawnien.Any(y => y.Id == c.Id));

                if (!allowed)
                    throw new InvalidOperationException("User nie posiada grupy dostepu przydzielonej materialowi.");

                if(System.DateTime.Now.Day > from.Day)
                    throw new InvalidOperationException("Nie mozna utworzyc wypozyczenia dla minionych dni.");

                var borrows= db.Wypozyczenies.Where(c => c.NarzedzieId == toolId);
                foreach (Wypozyczenie currentBorrow in borrows)
                {
                    if (currentBorrow.do_kiedy >= from || currentBorrow.do_kiedy >= to) {
                        throw new InvalidOperationException("Narzedzie jest wtedy zajete.");
                    }
                }
                
                borrow.User = user;
                borrow.Narzedzie = tool;
                    
                db.Wypozyczenies.Add(borrow);
                db.SaveChanges();
            }

            return borrow;
        }

        /*private static void RemoveOldDependency(int toolId, int userId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var dependency = db.Wypozyczenies.FirstOrDefault(c => c.UserId == userId &&
                    c.NarzedzieId == toolId);

                if (!EqualityComparer<Wypozyczenie>.Default.Equals(dependency, default(Wypozyczenie)))
                {
                    db.Wypozyczenies.Remove(dependency);
                    db.SaveChanges();
                }
            }
        }*/

        /*public static void updateActualReturnDate(int userId, int toolId, System.DateTime from, System.DateTime actualDate)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var borrow = db.Wypozyczenies.First(c => c.UserId == userId &&
                    c.NarzedzieId == toolId && c.od_kiedy == from);

                borrow.faktyczna_data_oddania = actualDate;
                db.SaveChanges();
            }
        }*/

        public static void updateActualReturnDate(int borrowId, System.DateTime actualDate)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var borrow = db.Wypozyczenies.First(c => c.Id == borrowId);

                borrow.faktyczna_data_oddania = actualDate;
                db.SaveChanges();
            }
        }

        public static List<Wypozyczenie> GetUserBorrowings(string userEmail)
        {
            List<Wypozyczenie> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Wypozyczenies.Where(c => c.UsersId == userEmail).ToList();
            }

            return list;
        }

        public static List<Wypozyczenie> GetToolBorrowings(int toolId)
        {
            List<Wypozyczenie> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Wypozyczenies.Where(c => c.NarzedzieId== toolId).ToList();
            }

            return list;
        }

        public static List<historia_wypozyczen> GetToolBorrowingsView(int toolId)
        {
            List<historia_wypozyczen> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.historia_wypozyczen.Where(c => c.NarzedzieId == toolId).ToList();
            }

            return list;
        }

        /*public static void DeleteBorrowing(int userId, int toolId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var borrow = db.Wypozyczenies.First(c => c.UserId == userId &&
                    c.NarzedzieId == toolId);

                db.Wypozyczenies.Remove(borrow);
                db.SaveChanges();
            }
        }*/

        public static void DeleteBorrowing(int borrowId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var borrow = db.Wypozyczenies.First(c => c.Id == borrowId);

                db.Wypozyczenies.Remove(borrow);
                db.SaveChanges();
            }
        }
    }
}