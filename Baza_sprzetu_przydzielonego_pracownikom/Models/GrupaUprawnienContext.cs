﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class GrupaUprawnienContext
    {
        public static Grupa_uprawnien AddNewGroup(int id, string description)
        {
            var group = new Grupa_uprawnien()
            {
                Id = id,
                opis = description
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                db.Grupa_uprawnien.Add(group);
                db.SaveChanges();
            }

            return group;
        }

        public static void DeleteGroup(int id)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var group = db.Grupa_uprawnien.First(c => c.Id == id);

                db.Grupa_uprawnien.Remove(group);
                db.SaveChanges();
            }
        }

        public static void UpdateGroupDescription(int id, string newDescription)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var group = db.Grupa_uprawnien.First(c => c.Id == id);

                group.opis = newDescription;
                db.SaveChanges();
            }
        }

        public static List<Grupa_uprawnien> GetAllGroups()
        {
            List<Grupa_uprawnien> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Grupa_uprawnien.ToList();
            }

            return list;
        }
    }
}