﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class ZabranieMaterialuContext
    {
        public static Zabranie_materialu AddTaking(string userEmail, int materialId, 
            DateTime when, int number)
        {
            //RemoveOldDependency(materialId, userId);

            var taking = new Zabranie_materialu()
            {
                kiedy = when,
                zabrana_ilosc = number,
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var user = db.Users.First(c => c.email == userEmail);
                var material = db.Materials.First(c => c.Id == materialId);

                if (number > material.ilosc_w_magazynie)
                {
                    throw new InvalidOperationException("Zabierana ilosc jest wieksza niz dostepna w magazynie");
                }

                bool allowed = material.Grupa_uprawnien.Any(c => user.Grupa_uprawnien.Any(y => y.Id == c.Id));

                if (!allowed)
                    throw new InvalidOperationException("User nie posiada grupy dostepu przydzielonej materialowi.");

                material.ilosc_w_magazynie -= number;

                taking.Material = material;
                taking.User = user;

                db.Zabranie_materialu.Add(taking);
                db.SaveChanges();
            }

            return taking;
        }

        /*private static void RemoveOldDependency(int materialId, int userId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var dependency = db.Zabranie_materialu.FirstOrDefault(c => c.UserId == userId && 
                    c.MaterialId == materialId);
                
                if (!EqualityComparer<Zabranie_materialu>.Default.Equals(dependency, default(Zabranie_materialu)))
                {
                    db.Zabranie_materialu.Remove(dependency);
                    db.SaveChanges();
                }                
            }
        }*/

        public static List<Zabranie_materialu> GetUserTakings(string userEmail)
        {
            List<Zabranie_materialu> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Zabranie_materialu.Where(c => c.UsersId == userEmail).ToList();
            }

            return list;
        }

        public static List<Zabranie_materialu> GetMaterialTakings(int materialId)
        {
            List<Zabranie_materialu> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Zabranie_materialu.Where(c => c.MaterialId == materialId).ToList();
            }

            return list;
        }

        /*public static void DeleteTaking(int userId, int materialId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var taking = db.Zabranie_materialu.First(c => c.UserId == userId &&
                    c.MaterialId == materialId);

                db.Zabranie_materialu.Remove(taking);
                db.SaveChanges();
            }
        }*/

        public static void DeleteTaking(int takeId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var taking = db.Zabranie_materialu.First(c => c.Id == takeId);

                db.Zabranie_materialu.Remove(taking);
                db.SaveChanges();
            }
        }
    }
}