﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class DostawcaContext
    {
        public static Dostawca AddNewSupplier(int nip, string name, int phone, Adres address)
        {
            var supplier = new Dostawca()
            {
                NIP = nip,
                nazwa_firmy = name,
                telefon = phone
            };

            supplier.Adre = address;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                db.Dostawcas.Add(supplier);
                db.SaveChanges();
            }

            return supplier;
        }

        public static void DeleteSupplier(int id)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supplier = db.Dostawcas.First(c => c.NIP == id);

                db.Dostawcas.Remove(supplier);
                db.SaveChanges();
            }
        }

        //TODO: update Supplier

        public static Dostawca GetSupplierByNIP(int id)
        {
            Dostawca supplier;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                supplier = db.Dostawcas.First(c => c.NIP == id);
            }

            return supplier;
        }

        public static List<Dostawca> GetAllSuppliers()
        {
            List<Dostawca> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Dostawcas.ToList();
            }

            return list;
        }
    }
}