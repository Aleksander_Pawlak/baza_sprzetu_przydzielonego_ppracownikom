﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class MaterialContext
    {
        public static Material AddNewMaterial(string name, int number, string unit)
        {
            var material = new Material()
            {
                nazwa = name,
                ilosc_w_magazynie = number,
                jednostka = unit
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                db.Materials.Add(material);
                db.SaveChanges();
            }

            return material;
        }

        public static void DeleteMaterial(int id)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var material = db.Materials.First(c => c.Id == id);

                db.Materials.Remove(material);
                db.SaveChanges();
            }
        }

        public static void AddMaterialPrivilegesGroup(int id, int groupId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var material = db.Materials.First(c => c.Id == id);
                var group = db.Grupa_uprawnien.First(c => c.Id == groupId);

                material.Grupa_uprawnien.Add(group);
                db.SaveChanges();
            }
        }

        //TODO: update Material

        public static Material GetMaterialById(int id)
        {
            Material material;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                material = db.Materials.First(c => c.Id == id);
            }

            return material;
        }

        public static List<Material> GetAllMaterials()
        {
            List<Material> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Materials.ToList();
            }

            return list;
        }

        public static List<Materialy> GetAllMaterialsView()
        {
            List<Materialy> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Materialies.ToList();
            }

            return list;
        }
    }
}