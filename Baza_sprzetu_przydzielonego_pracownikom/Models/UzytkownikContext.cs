﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class UserContext
    {
        public static User AddNewUser(string Pesel, string name, string surname, int age, string email, 
            int phone, string password, string privileges, Adres address)
        {
            if (privileges != "Pracownik")
            {
                throw new Exception("Forbidden privileges in creating user.");
            }

            var user = new User()
            {
                imie = name,
                nazwisko = surname,
                PESEL = Pesel,
                wiek = age,
                email = email,
                telefon = phone,
                haslo = password,
                userRole = privileges
            };

            user.Adre = address;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                db.Users.Add(user);
                //db.CreateUser(user.Id, user.email, user.haslo);
                //db.AddUserRole(user.Id, user.userRole);
                db.SaveChanges();
            }

            return user;
        }

        public static void DeleteUser(string email)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var user = db.Users.First(c => c.email == email);

                db.Users.Remove(user);
                //db.DeleteUser(user.Id, user.email);
                //db.DeleteUserRole(user.Id, user.userRole);
                db.SaveChanges();
            }
        }

        public static void AddUserPrivilegesGroup(string email, int groupId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var user = db.Users.First(c => c.email == email);
                var group = db.Grupa_uprawnien.First(c => c.Id == groupId);

                user.Grupa_uprawnien.Add(group);
                db.SaveChanges();
            }
        }

        public static void ChangeUserPrivileges(string userEmail, string roleName)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var user = db.Users.First(c => c.email == userEmail);

                //db.DeleteUserRole(user.Id, user.userRole);
                user.userRole = roleName;
                //db.AddUserRole(user.Id, user.userRole);

                db.SaveChanges();
            }
        }
        //TODO: update User

        public static User GetUserById(string userEmail)
        {
            User user;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                user = db.Users.First(c => c.email == userEmail);
            }

            return user;
        }

        public static List<User> GetAllUsers()
        {
            List<User> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Users.ToList();
            }

            return list;
        }

        public static List<Uzytkownicy> GetAllUsersView()
        {
            List<Uzytkownicy> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Uzytkownicies.ToList();
            }

            return list;
        }
    }
}