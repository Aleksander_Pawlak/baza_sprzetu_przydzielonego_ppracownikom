﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class AdresContext
    {
        public static Adres AddNewAddress(string street, int buildingNumber, int flatNumber, string postalCode, string city)
        {
            var address = new Adres()
            {
                ulica = street,
                numer_budynku = buildingNumber,
                numer_lokalu = flatNumber,
                kod_pocztowy = postalCode,
                miejscowosc = city
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                db.Adres.Add(address);
                db.SaveChanges();
            }

            return address;
        }

        public static void DeleteAddress(int id)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var address = db.Adres.First(c => c.Id == id);

                db.Adres.Remove(address);
                db.SaveChanges();
            }
        }

        public static Adres GetAddressById(int id)
        {
            Adres supplier;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                supplier = db.Adres.First(c => c.Id == id);
            }

            return supplier;
        }

        public static Adres GetAddressByValues(string street, int buildingNumber, int flatNumber, string postalCode, string city)
        {
            Adres addresss;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                addresss = db.Adres.FirstOrDefault(c => c.ulica == street && c.numer_budynku == buildingNumber &&
                c.numer_lokalu == flatNumber && c.kod_pocztowy == postalCode && c.miejscowosc == city);
            }

            return addresss;
        }

        public static List<Adres> GetAllAddresses()
        {
            List<Adres> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Adres.ToList();
            }

            return list;
        }

        public static Adres DefineAddress(string street, int buildingNumber, int flatNumber, string postalCode, string city)
        {
            Adres address;

            address = GetAddressByValues(street, buildingNumber, flatNumber, postalCode, city);

            if (EqualityComparer<Adres>.Default.Equals(address, default(Adres)))
            {
                address = AddNewAddress(street, buildingNumber, flatNumber, postalCode, city);
            }

            return address;
        }
    }
}