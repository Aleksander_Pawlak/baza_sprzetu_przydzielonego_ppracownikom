﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class TypContext
    {
        public static Typ AddNewType(string name, string description)
        {
            var type = new Typ()
            {
                typ1 = name,
                opis = description
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                db.Typs.Add(type);
                db.SaveChanges();
            }

            return type;
        }

        public static void DeleteType(string name)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var type = db.Typs.First(c => c.typ1 == name);

                db.Typs.Remove(type);
                db.SaveChanges();
            }
        }

        public static void UpdateTypeDescription(string name, string newDescription)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var type = db.Typs.First(c => c.typ1 == name);

                type.opis = newDescription;
                db.SaveChanges();
            }
        }

        public static List<Typ> GetAllTypes()
        {
            List<Typ> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Typs.ToList();
            }

            return list;
        }
    }
}