﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class DostawaMaterialuContext
    {
        public static Dostawa_materialu AddMaterialSupply(int supplierNIP, int materialId, DateTime date, int number)
        {
            var delivery = new Dostawa_materialu()
            {
                data = date,
                ilosc = number
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supplier = db.Dostawcas.First(c => c.NIP == supplierNIP);
                var material = db.Materials.First(c => c.Id == materialId);

                delivery.jednostka = material.jednostka;
                delivery.Dostawca = supplier;
                delivery.Material = material;

                material.ilosc_w_magazynie += number;

                db.Dostawa_materialu.Add(delivery);
                db.SaveChanges();
            }

            return delivery;
        }

        /*private static void RemoveOldDependency(int materialId, int supplierNIP)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var dependency = db.Dostawa_materialu.FirstOrDefault(c => c.DostawcaNIP == supplierNIP &&
                    c.MaterialId == materialId);

                if (!EqualityComparer<Dostawa_materialu>.Default.Equals(dependency, default(Dostawa_materialu)))
                {
                    db.Dostawa_materialu.Remove(dependency);
                    db.SaveChanges();
                }
            }
        }*/

        /*public static void DeleteSupply(int nip, int materialId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supply = db.Dostawa_materialu.First(c => c.DostawcaNIP == nip &&
                    c.MaterialId == materialId);

                db.Dostawa_materialu.Remove(supply);
                db.SaveChanges();
            }
        }*/

        public static void DeleteSupply(int supplyId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var supply = db.Dostawa_materialu.First(c => c.Id == supplyId);

                db.Dostawa_materialu.Remove(supply);
                db.SaveChanges();
            }
        }

        public static List<Dostawa_materialu> GetAllSupplierSupply(int nip)
        {
            List<Dostawa_materialu> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Dostawa_materialu.Where(c => c.DostawcaNIP == nip).ToList();
            }

            return list;
        }

        public static List<Dostawa_materialu> GetAllSMaterialSupply(int materialId)
        {
            List<Dostawa_materialu> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Dostawa_materialu.Where(c => c.MaterialId == materialId).ToList();
            }

            return list;
        }

        /*public static void UpdateSupplyDate(int supplierNIP, int materialId, System.DateTime newDate)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var DbSupply = db.Dostawa_materialu.First(c => c.DostawcaNIP == supplierNIP &&
                    c.MaterialId == materialId);

                DbSupply.data = newDate;
                db.SaveChanges();
            }
        }*/

        public static void UpdateSupplyDate(int supplyId, System.DateTime newDate)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var DbSupply = db.Dostawa_materialu.First(c => c.Id == supplyId);

                DbSupply.data = newDate;
                db.SaveChanges();
            }
        }
    }
}