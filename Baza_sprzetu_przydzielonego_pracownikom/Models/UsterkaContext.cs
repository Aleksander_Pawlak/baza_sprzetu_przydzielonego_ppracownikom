﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class UsterkaContext
    {
        public static Usterka AddFault(string userEmail, int toolId, string state)
        {
            //RemoveOldDependency(toolId, userId);

            var fault = new Usterka()
            {
                stan = state
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            { 
                var user = db.Users.First(c => c.email == userEmail);
                var tool = db.Narzedzies.First(c => c.Id == toolId);
                
                fault.User = user;
                fault.Narzedzie = tool;

                tool.dostepnosc = false;

                db.Usterkas.Add(fault);
                db.SaveChanges();
            }

            return fault;
        }

        /*private static void RemoveOldDependency(int toolId, int userId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var dependency = db.Usterkas.FirstOrDefault(c => c.UserId == userId &&
                    c.NarzedzieId == toolId);

                if (!EqualityComparer<Usterka>.Default.Equals(dependency, default(Usterka)))
                {
                    db.Usterkas.Remove(dependency);
                    db.SaveChanges();
                }
            }
        }*/

        /*public static void DeleteFalut(int userId, int toolId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var fault = db.Usterkas.First(c => c.UserId == userId &&
                    c.NarzedzieId == toolId);

                db.Usterkas.Remove(fault);
                db.SaveChanges();
            }
        }*/

        public static void DeleteFalut(int faulId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var fault = db.Usterkas.First(c => c.Id == faulId);

                db.Usterkas.Remove(fault);
                db.SaveChanges();
            }
        }

        public static List<Usterka> GetToolFaults(int toolId)
        {
            List<Usterka> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Usterkas.Where(c => c.NarzedzieId == toolId).ToList();
            }

            return list;
        }

    }
}