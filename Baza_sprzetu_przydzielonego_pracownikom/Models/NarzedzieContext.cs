﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class NarzedzieContext
    {
        public static Narzedzie AddNewTool(string name, string weight, string state, string parameters, string size, string typeName)
        {
            var tool = new Narzedzie()
            {
                nazwa = name,
                waga = weight,
                stan = state,
                parametry = parameters,
                rozmiar = size,
                dostepnosc = true
            };
                        
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var type = db.Typs.First(c => c.typ1 == typeName);
                tool.Typ = type;

                db.Narzedzies.Add(tool);
                db.SaveChanges();
            }

            return tool;
        }

        public static void ChangeToolAvailablity(int toolId, bool state)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var tool = db.Narzedzies.First(c => c.Id == toolId);

                tool.dostepnosc = state;
                db.SaveChanges();
            }
        }

        public static void DeleteTool(int id)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var tool = db.Narzedzies.First(c => c.Id == id);

                db.Narzedzies.Remove(tool);
                db.SaveChanges();
            }
        }

        public static void AddToolPrivilegesGroup(int id, Grupa_uprawnien group)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var tool = db.Narzedzies.First(c => c.Id == id);

                tool.Grupa_uprawnien.Add(group);
                db.SaveChanges();
            }
        }

        //TODO: update tool

        public static Narzedzie GetToolById(int id)
        {
            Narzedzie tool;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                tool = db.Narzedzies.First(c => c.Id == id);
            }

            return tool;
        }

        public static List<Narzedzie> GetAllTools()
        {
            List<Narzedzie> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Narzedzies.ToList();
            }

            return list;
        }

        public static List<Narzedzie> GetTypeTools(string type)
        {
            List<Narzedzie> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Narzedzies.Where(c => c.Typtyp == type).ToList();
            }

            return list;
        }


        public static List<Narzedzia> GetAllToolsView()
        {
            List<Narzedzia> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Narzedzias.ToList();
            }

            return list;
        }
    }
}