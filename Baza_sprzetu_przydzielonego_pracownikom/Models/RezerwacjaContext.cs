﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Baza_sprzetu_przydzielonego_pracownikom.Models
{
    public class RezerwacjaContext
    {
        public static Rezerwacja AddReservation(string userEmail, int toolId, DateTime from)
        {
            //RemoveOldDependency(toolId, userId);

            var reservation = new Rezerwacja()
            {
                od_kiedy = from
            };

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var user = db.Users.First(c => c.email == userEmail);
                var tool = db.Narzedzies.First(c => c.Id == toolId);
            
                //bool allowed = tool.Grupa_uprawnien.Any(c => user.Grupa_uprawnien.Any(y => y.Id == c.Id));

                //if (!allowed)
                //    throw new InvalidOperationException("User nie posiada grupy dostepu przydzielonej materialowi.");

                reservation.User = user;
                reservation.Narzedzie = tool;
                ///tghfgh
                db.Rezerwacjas.Add(reservation);
                db.SaveChanges();
            }

            return reservation;
        }

        /*private static void RemoveOldDependency(int toolId, int userId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var dependency = db.Rezerwacjas.FirstOrDefault(c => c.UserId == userId &&
                    c.NarzedzieId == toolId);

                if (!EqualityComparer<Rezerwacja>.Default.Equals(dependency, default(Rezerwacja)))
                {
                    db.Rezerwacjas.Remove(dependency);
                    db.SaveChanges();
                }
            }
        }*/

        /*public static void changeReservationDate(int userId, int toolId, System.DateTime from)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var reservation = db.Rezerwacjas.First(c => c.UserId == userId &&
                    c.NarzedzieId == toolId && c.od_kiedy == from);

                reservation.od_kiedy = from;
                db.SaveChanges();
            }
        }*/

        public static void changeReservationDate(int reservationId, System.DateTime from)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var reservation = db.Rezerwacjas.First(c => c.Id == reservationId);

                reservation.od_kiedy = from;
                db.SaveChanges();
            }
        }

        public static List<Rezerwacja> GetUserReservations(string userEmail)
        {
            List<Rezerwacja> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Rezerwacjas.Where(c => c.UsersId == userEmail).ToList();
            }

            return list;
        }

        public static List<Rezerwacja> GetToolReservations(int toolId)
        {
            List<Rezerwacja> list;

            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                list = db.Rezerwacjas.Where(c => c.NarzedzieId == toolId).ToList();
            }

            return list;
        }

        /*public static void DeleteReservation(int userId, int toolId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var reservation = db.Rezerwacjas.First(c => c.UserId == userId &&
                    c.NarzedzieId == toolId);

                db.Rezerwacjas.Remove(reservation);
                db.SaveChanges();
            }
        }*/

        public static void DeleteReservation(int reservationId)
        {
            using (var db = new Baza_sprzetu_przydzielonego_pracownikomEntities())
            {
                var reservation = db.Rezerwacjas.First(c => c.Id == reservationId);

                db.Rezerwacjas.Remove(reservation);
                db.SaveChanges();
            }
        }
    }
}