﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WyborNarzedzia.aspx.cs" Inherits="Baza_sprzetu_przydzielonego_pracownikom.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <head>
<style>
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto;
  background-color: #2196F3;
  padding: 10px;
  margin-left: 160px
}
.grid-item {
  background-color: rgba(255, 255, 255, 0.8);
  border: 1px solid rgba(0, 0, 0, 0.8);
  padding: 20px;
  font-size: 30px;
  text-align: center;
}
</style>
<script language="C#" runat="server">
      void ImageButton_Click(object sender, ImageClickEventArgs e) 
      {
          Response.Redirect("RezerwacjaNarzedzia.aspx");
      }
   </script>
</head>
<body>

<div class="grid-container">
    <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource1">
        <ItemTemplate>
            Id:
            <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' />
            <br />
            nazwa:
            <asp:Label ID="nazwaLabel" runat="server" Text='<%# Eval("nazwa") %>' />
            <br />
            waga:
            <asp:Label ID="wagaLabel" runat="server" Text='<%# Eval("waga") %>' />

             <a href="/RezerwacjaNarzedzia?idTool=<%# Eval("Id") %>">Rezerwuj</a>
<br />
            <br />
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Baza_sprzetu_przydzielonego_pracownikomConnectionStringsss %>" SelectCommand="SELECT [Id], [nazwa], [waga] FROM [Narzedzie] WHERE ([Typtyp] = @Typtyp)">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="" Name="Typtyp" QueryStringField="katId" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</div>

</body>
</html>

</form>
</asp:Content>

