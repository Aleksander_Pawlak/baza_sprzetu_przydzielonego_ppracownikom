﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Modyfikuj.aspx.cs" Inherits="Baza_sprzetu_przydzielonego_pracownikom.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <head>
        <style>
            .grid-container {
                display: grid;
                grid-template-columns: auto auto auto;
                background-color: #2196F3;
                padding: 10px;
                margin-left: 160px
            }

            .grid-item {
                background-color: rgba(255, 255, 255, 0.8);
                border: 1px solid rgba(0, 0, 0, 0.8);
                padding: 20px;
                font-size: 30px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="grid-container">

            <asp:Button ID="Button1" runat="server" Height="187px" Text="Użytkowników" Width="163px" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Height="187px" Text="Narzędzia" Width="168px" OnClick="Button2_Click" />
            <asp:Button ID="Button3" runat="server" Height="187px" Text="Materiały" Width="165px" OnClick="Button3_Click" />

        </div>

    </body>
    </html>

</form>
</asp:Content>
