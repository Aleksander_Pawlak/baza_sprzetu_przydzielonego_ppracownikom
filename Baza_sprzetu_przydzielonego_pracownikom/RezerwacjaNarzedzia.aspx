﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RezerwacjaNarzedzia.aspx.cs" Inherits="Baza_sprzetu_przydzielonego_pracownikom.RezerwacjaNarzedzia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <head>
        <style>
            .grid-container {
                display: grid;
                grid-template-columns: auto auto auto;
                background-color: #2196F3;
                padding: 10px;
                margin-left: 160px
            }

            .grid-item {
                background-color: rgba(255, 255, 255, 0.8);
                border: 1px solid rgba(0, 0, 0, 0.8);
                padding: 20px;
                font-size: 30px;
                text-align: center;
            }
        </style>
    </head>
    <body>

        <div class="grid-container">
         
              <asp:Table ID="Info" runat="server">
   <asp:TableRow>
      <asp:TableCell  Text="Nazwa: "/>
      <asp:TableCell  Text="Młotek" />
   </asp:TableRow>
        <asp:TableRow>
      <asp:TableCell  Text=" Katygiria: "/>
      <asp:TableCell  Text=" BUM BUM"/>
   </asp:TableRow>
   <asp:TableRow>
      <asp:TableCell  Text="Podkatygoria: "/>
      <asp:TableCell  Text="małe BUM BUM"/>
   </asp:TableRow>
        <asp:TableRow>
      <asp:TableCell  Text="Grupa Uprawnień: "/>
      <asp:TableCell  Text="NIE dzieci "/>
   </asp:TableRow>
        <asp:TableRow>
      <asp:TableCell  Text="ID: "/>
      <asp:TableCell  Text="997997"/>
   </asp:TableRow>
                    </asp:Table>
            <asp:DataList ID="historiaWypozyczen" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" DataSourceID="SqlDataSource1">
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <ItemTemplate>
                    Id:
                    <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' />
                    <br />
                    nazwa:
                    <asp:Label ID="nazwaLabel" runat="server" Text='<%# Eval("nazwa") %>' />
                    <br />
                    waga:
                    <asp:Label ID="wagaLabel" runat="server" Text='<%# Eval("waga") %>' />
                    <br />
                    <br />
                </ItemTemplate>
                <SelectedItemStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            </asp:DataList>
              <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Baza_sprzetu_przydzielonego_pracownikomConnectionStringsss %>" SelectCommand="SELECT [Id], [nazwa], [waga] FROM [Narzedzie] WHERE ([Typtyp] = @Typtyp)">
                  <SelectParameters>
                      <asp:QueryStringParameter Name="Typtyp" QueryStringField="katId" Type="String" />
                  </SelectParameters>
              </asp:SqlDataSource>
            <asp:Calendar ID="dataOd" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px">
                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                <NextPrevStyle VerticalAlign="Bottom" />
                <OtherMonthDayStyle ForeColor="#808080" />
                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                <SelectorStyle BackColor="#CCCCCC" />
                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                <WeekendDayStyle BackColor="#FFFFCC" />
            </asp:Calendar>
            <asp:Calendar ID="dataDo" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px">
                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                <NextPrevStyle VerticalAlign="Bottom" />
                <OtherMonthDayStyle ForeColor="#808080" />
                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                <SelectorStyle BackColor="#CCCCCC" />
                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                <WeekendDayStyle BackColor="#FFFFCC" />
            </asp:Calendar>
            <div class="grid-container">
                <asp:Button ID="RezerwujModyfikuj" runat="server" Text="Rezerwuj/Modyfikuj" OnClick="RezerwujModyfikuj_Click" />
                <asp:Button ID="PobierzZdaj" runat="server" Text="Pobierz/Zdaj" OnClick="PobierzZdaj_Click" />
                <asp:Button ID="ZgłośUsterkę" runat="server" Text="Zgłoś Usterkę" OnClick="ZgłośUsterkę_Click" />
                <asp:Button ID="UsunUsterkeAdmin" runat="server" Text="Usuń usterkę" BorderColor="Red" OnClick="UsunUsterkeAdmin_Click" />
            </div>
        </div>

    </body>
    </html>

</form>
</asp:Content>
